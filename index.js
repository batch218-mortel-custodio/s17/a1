/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function printWelcomeMessage(){
	let fullName = prompt("Enter your full name: ");
	let userAge = prompt("Enter your age in years: ");
	let userLocation = prompt("Enter your location: ");

	console.log("Hello, "+ fullName + "!");
	console.log("You are "+ userAge +" years old.");
	console.log("You live in "+ userLocation + ".");
	}

	printWelcomeMessage();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function listFavArtist(){
		console.log("1. Jaya");
		console.log("2. Jano Gibbs");
		console.log("3. Renz Verano");
		console.log("4. Sarah Geronimo");
		console.log("5. Andrew E.");
	}

	listFavArtist();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	function listFavMovies(){
		let movieName1 = "Hustle";
		let tomatoRating1 = "93%";
		let movieName2 = "Day Shift";
		let tomatoRating2 = "56%";
		let movieName3 = "Adam Project";
		let tomatoRating3 = "67%";
		let movieName4 = "The Irishman";
		let tomatoRating4 = "95%";
		let movieName5 = "Margin Call";
		let tomatoRating5 = "87%";
		console.log("1. "+ movieName1);
			console.log("Rotten Tomatoes Rating: "+ tomatoRating1);
		console.log("2. "+ movieName2);
			console.log("Rotten Tomatoes Rating: "+ tomatoRating2);
		console.log("3. "+ movieName3);
			console.log("Rotten Tomatoes Rating: "+ tomatoRating3);
		console.log("4. "+ movieName4);
			console.log("Rotten Tomatoes Rating: "+ tomatoRating4);
		console.log("5. "+ movieName5);
			console.log("Rotten Tomatoes Rating: "+ tomatoRating5);

	}

	listFavMovies();	
	

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);
